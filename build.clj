(ns build
  (:require [clojure.tools.build.api :as b]
            [build.taxonomy :as taxonomy]
            [build.analyzer :as analyzer]
            [build.backends.python :as python]
            [clojure.tools.logging :as log]))

(def taxonomy-settings taxonomy/default-settings)

(defn clean [_]
  (b/delete {:path (:data-root taxonomy-settings)}))

(defn get-taxonomy-data [_]
  (taxonomy/download-taxonomy taxonomy-settings))

(def libraries-to-build [{:key :python
                          :builder python/build-and-validate
                          :dst-path "jobtech-taxonomy-library-python"}])

(defn build-library [cg-data {:keys [key builder dst-path]}]
  (log/info "Build library for" key)
  (for [err (builder dst-path cg-data)]
    (assoc err :key key)))

(defn build-libraries [_]
  (let [_ (log/info "Prepare taxonomy data.")
        version-data (-> taxonomy-settings
                         taxonomy/download-index-file
                         taxonomy/load-taxonomy-versions)
        cg-data (analyzer/build-codegen-data version-data)
        errors (into [] (mapcat #(build-library cg-data %)) libraries-to-build)
        failed-backends (into #{} (map :key) errors)
        successful-backends (into #{} (comp (map :key) (remove failed-backends)) libraries-to-build)]
    (log/info "Successful backends:" successful-backends)
    (if (seq failed-backends)
      (log/error "Failed backends:" failed-backends)
      (log/info "No failures."))))

(defn ^:export deploy [_]
  (clean nil)
  (get-taxonomy-data nil)
  (build-libraries nil))
