from jobtech_taxonomy_library_python.generated.data import build_taxonomy

taxonomy = build_taxonomy()

print(taxonomy)


concepts = taxonomy.concept_list()
types = taxonomy.concept_type_list()

#print(types)
print(types)

c = concepts[0]
print(f"\n\nCONCEPT:")
print(c)
the_type = c.concept_type()
print(f"TYPE: {the_type}, which has {len(the_type.concepts())} concepts.")

print(f"\n\n\n**** BROADER: {c.broader()}")

vt = taxonomy
while vt != None:
    print(f"VERSION: {vt.version}")
    vt = vt.previous


print("\n\n***************************** SUCCESS *****************************")
