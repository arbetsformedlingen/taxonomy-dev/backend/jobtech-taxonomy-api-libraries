def keep_first(f, coll):
    for x in coll:
        result = f(x)
        if result is not None:
            return result

def keep_last(f, coll):
    return keep_first(f, reversed(coll))

class TaxonomyVersionMapBase:
    def _initialize(self, initial_taxonomy):
        self.taxonomy_version_map = {}
        self.versions = []
        self._latest = initial_taxonomy
        self._edited = False

    def taxonomy_history(self):
        result = [self.taxonomy_version_map[version] for version in self.versions]
        if self._edited:
            result.append(self._latest)
        return result

    def __getitem__(self, i):
        return self._latest if i is None else self.taxonomy_version_map[i]

    def latest(self):
        return self._latest if self._edited else self.taxonomy_version_map[self.versions[-1]]

    def modify_latest(self):
        self._edited = True
        return self._latest
    
    def complete_version(self, v):
        released_version = self._latest.deep_copy()
        released_version.version = v
        self.taxonomy_version_map[v] = released_version
        self.versions.append(v)
        self._edited = False

    def apply_accumulators(self, accumulators):
        for acc in accumulators:
            acc.apply(self)
        return self

class TaxonomyBase:
    def __repr__(self):
        info = ", ".join([f"{acc.name()}:{len(acc.get(self))}" for acc in self.accessors()])
        return f"Taxonomy(version={self.version}, {info})"
    
class ItemData:
    "Base class for classes that hold data"

    def accessors(self):
        raise NotImplementedError("accumulators")
    
    def data(self):
        dst = {}
        for acc in self.accessors():
            dst[acc.name()] = acc.get(self)
        return dst
        

def _has_value(x):
    if x == None:
        return False
    if isinstance(x, set) and len(x) == 0:
        return False
    return True

class ItemBase:
    "Base class for concepts and concept types"
    def id(self):
        return self.item_id

    def exists_or_none(self):
        if self.exists():
            return self

    def version(self):
        return self.taxonomy.version
    
    def item_type_name(self):
        raise NotImplementedError("item_type_name")

    def with_id(self):
        raise NotImplementedError("with_id")

    def with_ids(self, ids):
        return [self.with_id(i) for i in ids]

    def lookup_data(self):
        raise NotImplementedError("lookup_data")

    def __repr__(self):
        data = self.data()
        info = ", ".join([f"{acc.name()}={v}"
                          for acc in data.accessors()
                          for v in [acc.get(data)]
                          if _has_value(v)])
        return f"{self.item_type_name()}(id={self.item_id}, {info})"
    
    def data(self):
        if self._data is None:
            self._data = self.lookup_data()
            if self._data is None:
                raise RuntimeError(f"Lookup of {self.item_id} failed")
        return self._data

class ConceptTypeBase(ItemBase):
    def concepts(self):
        return [self.taxonomy.get_concept(k)
                for k, cdata in self.taxonomy.concepts.items()
                if cdata.type == self.item_id]

class ConceptBase(ItemBase):
    def concept_type(self):
        return self.taxonomy.get_concept_type(self.data().type)

class ItemAccumulator:
    """This class is used by the generated code for expressing updates of concepts or concept types in the taxonomy."""
    
    def collection_accessor(self):
        """Returns an accessor for accessing the collection of the Taxonomy that this accumulator should be applied to (concepts or concept types)"""
        raise NotImplementedError("collection_accessor")

    def new_data(self):
        """Returns a new instance of the data item that this accumulator populates."""
        raise NotImplementedError("new_data")
    
    def apply(self, taxonomy_version_map):
        """Updates the Taxonomy with the changes of this accumulator"""
        item_map = self.collection_accessor().get(taxonomy_version_map.modify_latest())
        if self.deleted:
            if self.item_id in item_map:
                del item_map[self.item_id]
        else:
            item = item_map.get(self.item_id)
            if item is None:
                item = self.new_data()
                item_map[self.item_id] = item
            for accumulator in self.accumulators():
                accumulator.apply(item)

class FieldAccumulator:
    """This is the base class for accumulators that update a single field."""
    def __init__(self, parent_acc, accessor):
        self._parent_acc = parent_acc
        self._accessor = accessor
        self._data = []
        self._called = False

    def last_value(self):
        return self._data[-1]
        
    def update_target(self, target):
        raise NotImplementedError("update_target")
    
    def __call__(self, *args):
        self._called = True
        self._data.extend(args)
        return self._parent_acc
    
    def apply(self, target):
        if self._called:
            self.update_target(target)

class SetAccumulator(FieldAccumulator):
    """This accumulator sets a value of a field."""
    def __init__(self, parent_acc, accessor):
        super().__init__(parent_acc, accessor)

    def update_target(self, target):
        self._accessor.set(target, self.last_value())

class UnsetAccumulator(FieldAccumulator):
    """This accumulator unsets a value of a field."""
    def __init__(self, parent_acc, accessor):
        super().__init__(parent_acc, accessor)

    def update_target(self, target):
        self._accessor.set(target, None)

class AddAccumulator(FieldAccumulator):
    """This accumulator adds a value to a field holding a set."""
    def __init__(self, parent_acc, accessor):
        super().__init__(parent_acc, accessor)

    def update_target(self, target):
        coll = self._accessor.get(target)
        for x in self._data:
            coll.add(x)
        
class RemoveAccumulator(FieldAccumulator):
    """This accumulator removes a value from a field holding a set."""
    def __init__(self, parent_acc, accessor):
        super().__init__(parent_acc, accessor)

    def update_target(self, target):
        coll = self._accessor.get(target)
        for x in self._data:
            if x in coll:
                coll.remove(x)
            else:
                print("**WARNING**: Failed to remove item from field.")

class CompleteVersion:
    """This class marks the completion of a taxonomy version."""
    def __init__(self, version):
        self._version = version

    def apply(self, taxonomy_version_map):
        taxonomy_version_map.complete_version(self._version)

def deep_copy_map(m):
    return {k:v.deep_copy() for (k, v) in m.items()}

class ConstantAccessor:
    def __init__(self, name, value):
        self._name = name
        self._value = value
    
    def get(self, x):
        return self._value

    def name(self):
        return self._name

    def set(self, dst, x):
        pass
