
import jobtech_taxonomy_library_python.codegen_utils as codegen_utils
import jobtech_taxonomy_library_python.generated.models as models
import jobtech_taxonomy_library_python.generated.accessors as accessors




class ConceptTypeAccumulator(codegen_utils.ItemAccumulator):
    def __init__(self, item_id):
        
        self.item_id=item_id
        self.deleted=False
        
        self.ls=codegen_utils.SetAccumulator(self, accessors.accessor_label_sv)
        self._ls=codegen_utils.UnsetAccumulator(self, accessors.accessor_label_sv)
        
        self.le=codegen_utils.SetAccumulator(self, accessors.accessor_label_en)
        self._le=codegen_utils.UnsetAccumulator(self, accessors.accessor_label_en)
    def collection_accessor(self):
        return accessors.accessor_concept_types
    def new_data(self):
        return models.ConceptTypeData(self.item_id)
    def accumulators(self):
        return [self.ls,self._ls,self.le,self._le]
    def delete(self):
        self.deleted=True; return self;
def t(item_id):
    return ConceptTypeAccumulator(item_id)




class ConceptAccumulator(codegen_utils.ItemAccumulator):
    def __init__(self, item_id):
        
        self.item_id=item_id
        self.deleted=False
        
        self.hl=codegen_utils.AddAccumulator(self, accessors.accessor_hidden_labels)
        self._hl=codegen_utils.RemoveAccumulator(self, accessors.accessor_hidden_labels)
        
        self.df=codegen_utils.SetAccumulator(self, accessors.accessor_definition)
        self._df=codegen_utils.UnsetAccumulator(self, accessors.accessor_definition)
        
        self.p=codegen_utils.SetAccumulator(self, accessors.accessor_preferred_label)
        self._p=codegen_utils.UnsetAccumulator(self, accessors.accessor_preferred_label)
        
        self.sc2012=codegen_utils.SetAccumulator(self, accessors.accessor_ssyk_code_2012)
        self._sc2012=codegen_utils.UnsetAccumulator(self, accessors.accessor_ssyk_code_2012)
        
        self.enc2007=codegen_utils.SetAccumulator(self, accessors.accessor_eures_nace_code_2007)
        self._enc2007=codegen_utils.UnsetAccumulator(self, accessors.accessor_eures_nace_code_2007)
        
        self.t=codegen_utils.SetAccumulator(self, accessors.accessor_type)
        self._t=codegen_utils.UnsetAccumulator(self, accessors.accessor_type)
        
        self.r=codegen_utils.AddAccumulator(self, accessors.accessor_related)
        self._r=codegen_utils.RemoveAccumulator(self, accessors.accessor_related)
        
        self.sd=codegen_utils.SetAccumulator(self, accessors.accessor_short_description)
        self._sd=codegen_utils.UnsetAccumulator(self, accessors.accessor_short_description)
        
        self.pc=codegen_utils.AddAccumulator(self, accessors.accessor_possible_combinations)
        self._pc=codegen_utils.RemoveAccumulator(self, accessors.accessor_possible_combinations)
        
        self.eu=codegen_utils.SetAccumulator(self, accessors.accessor_esco_uri)
        self._eu=codegen_utils.UnsetAccumulator(self, accessors.accessor_esco_uri)
        
        self.bm=codegen_utils.AddAccumulator(self, accessors.accessor_broad_match)
        self._bm=codegen_utils.RemoveAccumulator(self, accessors.accessor_broad_match)
        
        self.n=codegen_utils.AddAccumulator(self, accessors.accessor_narrower)
        self._n=codegen_utils.RemoveAccumulator(self, accessors.accessor_narrower)
        
        self.b=codegen_utils.AddAccumulator(self, accessors.accessor_broader)
        self._b=codegen_utils.RemoveAccumulator(self, accessors.accessor_broader)
        
        self.nm=codegen_utils.AddAccumulator(self, accessors.accessor_narrow_match)
        self._nm=codegen_utils.RemoveAccumulator(self, accessors.accessor_narrow_match)
        
        self.ec2014=codegen_utils.SetAccumulator(self, accessors.accessor_eures_code_2014)
        self._ec2014=codegen_utils.UnsetAccumulator(self, accessors.accessor_eures_code_2014)
        
        self.em=codegen_utils.AddAccumulator(self, accessors.accessor_exact_match)
        self._em=codegen_utils.RemoveAccumulator(self, accessors.accessor_exact_match)
        
        self.rp=codegen_utils.AddAccumulator(self, accessors.accessor_replaces)
        self._rp=codegen_utils.RemoveAccumulator(self, accessors.accessor_replaces)
        
        self.u=codegen_utils.SetAccumulator(self, accessors.accessor_uri)
        self._u=codegen_utils.UnsetAccumulator(self, accessors.accessor_uri)
        
        self.a=codegen_utils.AddAccumulator(self, accessors.accessor_alternative_labels)
        self._a=codegen_utils.RemoveAccumulator(self, accessors.accessor_alternative_labels)
        
        self.uc=codegen_utils.AddAccumulator(self, accessors.accessor_unlikely_combinations)
        self._uc=codegen_utils.RemoveAccumulator(self, accessors.accessor_unlikely_combinations)
        
        self.sy=codegen_utils.AddAccumulator(self, accessors.accessor_substituted_by)
        self._sy=codegen_utils.RemoveAccumulator(self, accessors.accessor_substituted_by)
        
        self.cm=codegen_utils.AddAccumulator(self, accessors.accessor_close_match)
        self._cm=codegen_utils.RemoveAccumulator(self, accessors.accessor_close_match)
        
        self.s=codegen_utils.AddAccumulator(self, accessors.accessor_substitutes)
        self._s=codegen_utils.RemoveAccumulator(self, accessors.accessor_substitutes)
    def collection_accessor(self):
        return accessors.accessor_concepts
    def new_data(self):
        return models.ConceptData(self.item_id)
    def accumulators(self):
        return [self.hl,self._hl,self.df,self._df,self.p,self._p,self.sc2012,self._sc2012,self.enc2007,self._enc2007,self.t,self._t,self.r,self._r,self.sd,self._sd,self.pc,self._pc,self.eu,self._eu,self.bm,self._bm,self.n,self._n,self.b,self._b,self.nm,self._nm,self.ec2014,self._ec2014,self.em,self._em,self.rp,self._rp,self.u,self._u,self.a,self._a,self.uc,self._uc,self.sy,self._sy,self.cm,self._cm,self.s,self._s]
    def delete(self):
        self.deleted=True; return self;
def c(item_id):
    return ConceptAccumulator(item_id)


