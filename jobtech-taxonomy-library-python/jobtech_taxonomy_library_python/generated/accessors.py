
class Accessor_hidden_labels:
    def name(self):
        return "hidden_labels"
    def get(self, src):
        return src.hidden_labels
    def set(self, dst, x):
        dst.hidden_labels=x
accessor_hidden_labels = Accessor_hidden_labels()



class Accessor_definition:
    def name(self):
        return "definition"
    def get(self, src):
        return src.definition
    def set(self, dst, x):
        dst.definition=x
accessor_definition = Accessor_definition()



class Accessor_preferred_label:
    def name(self):
        return "preferred_label"
    def get(self, src):
        return src.preferred_label
    def set(self, dst, x):
        dst.preferred_label=x
accessor_preferred_label = Accessor_preferred_label()



class Accessor_ssyk_code_2012:
    def name(self):
        return "ssyk_code_2012"
    def get(self, src):
        return src.ssyk_code_2012
    def set(self, dst, x):
        dst.ssyk_code_2012=x
accessor_ssyk_code_2012 = Accessor_ssyk_code_2012()



class Accessor_eures_nace_code_2007:
    def name(self):
        return "eures_nace_code_2007"
    def get(self, src):
        return src.eures_nace_code_2007
    def set(self, dst, x):
        dst.eures_nace_code_2007=x
accessor_eures_nace_code_2007 = Accessor_eures_nace_code_2007()



class Accessor_concept_types:
    def name(self):
        return "concept_types"
    def get(self, src):
        return src.concept_types
    def set(self, dst, x):
        dst.concept_types=x
accessor_concept_types = Accessor_concept_types()



class Accessor_label_sv:
    def name(self):
        return "label_sv"
    def get(self, src):
        return src.label_sv
    def set(self, dst, x):
        dst.label_sv=x
accessor_label_sv = Accessor_label_sv()



class Accessor_type:
    def name(self):
        return "type"
    def get(self, src):
        return src.type
    def set(self, dst, x):
        dst.type=x
accessor_type = Accessor_type()



class Accessor_related:
    def name(self):
        return "related"
    def get(self, src):
        return src.related
    def set(self, dst, x):
        dst.related=x
accessor_related = Accessor_related()



class Accessor_short_description:
    def name(self):
        return "short_description"
    def get(self, src):
        return src.short_description
    def set(self, dst, x):
        dst.short_description=x
accessor_short_description = Accessor_short_description()



class Accessor_possible_combinations:
    def name(self):
        return "possible_combinations"
    def get(self, src):
        return src.possible_combinations
    def set(self, dst, x):
        dst.possible_combinations=x
accessor_possible_combinations = Accessor_possible_combinations()



class Accessor_concepts:
    def name(self):
        return "concepts"
    def get(self, src):
        return src.concepts
    def set(self, dst, x):
        dst.concepts=x
accessor_concepts = Accessor_concepts()



class Accessor_label_en:
    def name(self):
        return "label_en"
    def get(self, src):
        return src.label_en
    def set(self, dst, x):
        dst.label_en=x
accessor_label_en = Accessor_label_en()



class Accessor_esco_uri:
    def name(self):
        return "esco_uri"
    def get(self, src):
        return src.esco_uri
    def set(self, dst, x):
        dst.esco_uri=x
accessor_esco_uri = Accessor_esco_uri()



class Accessor_broad_match:
    def name(self):
        return "broad_match"
    def get(self, src):
        return src.broad_match
    def set(self, dst, x):
        dst.broad_match=x
accessor_broad_match = Accessor_broad_match()



class Accessor_narrower:
    def name(self):
        return "narrower"
    def get(self, src):
        return src.narrower
    def set(self, dst, x):
        dst.narrower=x
accessor_narrower = Accessor_narrower()



class Accessor_broader:
    def name(self):
        return "broader"
    def get(self, src):
        return src.broader
    def set(self, dst, x):
        dst.broader=x
accessor_broader = Accessor_broader()



class Accessor_narrow_match:
    def name(self):
        return "narrow_match"
    def get(self, src):
        return src.narrow_match
    def set(self, dst, x):
        dst.narrow_match=x
accessor_narrow_match = Accessor_narrow_match()



class Accessor_eures_code_2014:
    def name(self):
        return "eures_code_2014"
    def get(self, src):
        return src.eures_code_2014
    def set(self, dst, x):
        dst.eures_code_2014=x
accessor_eures_code_2014 = Accessor_eures_code_2014()



class Accessor_exact_match:
    def name(self):
        return "exact_match"
    def get(self, src):
        return src.exact_match
    def set(self, dst, x):
        dst.exact_match=x
accessor_exact_match = Accessor_exact_match()



class Accessor_replaces:
    def name(self):
        return "replaces"
    def get(self, src):
        return src.replaces
    def set(self, dst, x):
        dst.replaces=x
accessor_replaces = Accessor_replaces()



class Accessor_uri:
    def name(self):
        return "uri"
    def get(self, src):
        return src.uri
    def set(self, dst, x):
        dst.uri=x
accessor_uri = Accessor_uri()



class Accessor_alternative_labels:
    def name(self):
        return "alternative_labels"
    def get(self, src):
        return src.alternative_labels
    def set(self, dst, x):
        dst.alternative_labels=x
accessor_alternative_labels = Accessor_alternative_labels()



class Accessor_unlikely_combinations:
    def name(self):
        return "unlikely_combinations"
    def get(self, src):
        return src.unlikely_combinations
    def set(self, dst, x):
        dst.unlikely_combinations=x
accessor_unlikely_combinations = Accessor_unlikely_combinations()



class Accessor_substituted_by:
    def name(self):
        return "substituted_by"
    def get(self, src):
        return src.substituted_by
    def set(self, dst, x):
        dst.substituted_by=x
accessor_substituted_by = Accessor_substituted_by()



class Accessor_close_match:
    def name(self):
        return "close_match"
    def get(self, src):
        return src.close_match
    def set(self, dst, x):
        dst.close_match=x
accessor_close_match = Accessor_close_match()



class Accessor_substitutes:
    def name(self):
        return "substitutes"
    def get(self, src):
        return src.substitutes
    def set(self, dst, x):
        dst.substitutes=x
accessor_substitutes = Accessor_substitutes()


