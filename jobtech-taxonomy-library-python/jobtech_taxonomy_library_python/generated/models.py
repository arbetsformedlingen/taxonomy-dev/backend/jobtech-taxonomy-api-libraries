

import jobtech_taxonomy_library_python.codegen_utils as codegen_utils

import jobtech_taxonomy_library_python.generated.accessors as accessors
from copy import copy

class TaxonomyVersionMap(codegen_utils.TaxonomyVersionMapBase):
    def __init__(self):
        self._initialize(Taxonomy())
    def latest_concept_type(self, i):
        return codegen_utils.keep_last(lambda tax: ConceptType(self, tax, i).exists_or_none(), self.taxonomy_history())
    def concept_type_history(self, i):
        return ConceptType(self, self.latest, i).history()
    def concept_type_id_set(self):
        return {i for tax in self.taxonomy_history() for i in tax.concept_types.keys()}
    def latest_concept(self, i):
        return codegen_utils.keep_last(lambda tax: Concept(self, tax, i).exists_or_none(), self.taxonomy_history())
    def concept_history(self, i):
        return Concept(self, self.latest, i).history()
    def concept_id_set(self):
        return {i for tax in self.taxonomy_history() for i in tax.concepts.keys()}

class Taxonomy(codegen_utils.TaxonomyBase):
    def __init__(self):
        self.version = None
        self.concept_types={}
        self.concepts={}
    def deep_copy(self):
        dst = Taxonomy()
        dst.version = self.version
        dst.concept_types= codegen_utils.deep_copy_map(self.concept_types)
        dst.concepts= codegen_utils.deep_copy_map(self.concepts)
        return dst
    def accessors(self):
        return [accessors.accessor_concept_types,accessors.accessor_concepts]
    def get_concept_type(self, i):
        return ConceptType(self, self, i)
    def concept_type_keys(self):
        return self.concept_types.keys()
    def concept_type_list(self):
        return [self.get_concept_type(x) for x in self.concept_types.keys()]
    def get_concept(self, i):
        return Concept(self, self, i)
    def concept_keys(self):
        return self.concepts.keys()
    def concept_list(self):
        return [self.get_concept(x) for x in self.concepts.keys()]


class ConceptTypeData(codegen_utils.ItemData):
    def __init__(self, item_id):
        self.item_id=item_id
        self.label_sv=None
        self.label_en=None
    def accessors(self):
        return [codegen_utils.ConstantAccessor('id', self.item_id),accessors.accessor_label_sv,accessors.accessor_label_en]
    def deep_copy(self):
        dst = ConceptTypeData(self.item_id)
        dst.label_sv=copy(self.label_sv)
        dst.label_en=copy(self.label_en)
        return dst

class ConceptData(codegen_utils.ItemData):
    def __init__(self, item_id):
        self.item_id=item_id
        self.hidden_labels=set()
        self.definition=None
        self.preferred_label=None
        self.ssyk_code_2012=None
        self.eures_nace_code_2007=None
        self.type=None
        self.related=set()
        self.short_description=None
        self.possible_combinations=set()
        self.esco_uri=None
        self.broad_match=set()
        self.narrower=set()
        self.broader=set()
        self.narrow_match=set()
        self.eures_code_2014=None
        self.exact_match=set()
        self.replaces=set()
        self.uri=None
        self.alternative_labels=set()
        self.unlikely_combinations=set()
        self.substituted_by=set()
        self.close_match=set()
        self.substitutes=set()
    def accessors(self):
        return [codegen_utils.ConstantAccessor('id', self.item_id),accessors.accessor_hidden_labels,accessors.accessor_definition,accessors.accessor_preferred_label,accessors.accessor_ssyk_code_2012,accessors.accessor_eures_nace_code_2007,accessors.accessor_type,accessors.accessor_related,accessors.accessor_short_description,accessors.accessor_possible_combinations,accessors.accessor_esco_uri,accessors.accessor_broad_match,accessors.accessor_narrower,accessors.accessor_broader,accessors.accessor_narrow_match,accessors.accessor_eures_code_2014,accessors.accessor_exact_match,accessors.accessor_replaces,accessors.accessor_uri,accessors.accessor_alternative_labels,accessors.accessor_unlikely_combinations,accessors.accessor_substituted_by,accessors.accessor_close_match,accessors.accessor_substitutes]
    def deep_copy(self):
        dst = ConceptData(self.item_id)
        dst.hidden_labels=copy(self.hidden_labels)
        dst.definition=copy(self.definition)
        dst.preferred_label=copy(self.preferred_label)
        dst.ssyk_code_2012=copy(self.ssyk_code_2012)
        dst.eures_nace_code_2007=copy(self.eures_nace_code_2007)
        dst.type=copy(self.type)
        dst.related=copy(self.related)
        dst.short_description=copy(self.short_description)
        dst.possible_combinations=copy(self.possible_combinations)
        dst.esco_uri=copy(self.esco_uri)
        dst.broad_match=copy(self.broad_match)
        dst.narrower=copy(self.narrower)
        dst.broader=copy(self.broader)
        dst.narrow_match=copy(self.narrow_match)
        dst.eures_code_2014=copy(self.eures_code_2014)
        dst.exact_match=copy(self.exact_match)
        dst.replaces=copy(self.replaces)
        dst.uri=copy(self.uri)
        dst.alternative_labels=copy(self.alternative_labels)
        dst.unlikely_combinations=copy(self.unlikely_combinations)
        dst.substituted_by=copy(self.substituted_by)
        dst.close_match=copy(self.close_match)
        dst.substitutes=copy(self.substitutes)
        return dst


class ConceptType(codegen_utils.ConceptTypeBase):
    def __init__(self, taxonomy_version_map, taxonomy, item_id):
        assert(isinstance(item_id, str))
        self.taxonomy_version_map = taxonomy_version_map
        self.taxonomy = taxonomy
        self.item_id = item_id
        self._data = None
    def history_of(self, taxonomies):
        return (x for tax in taxonomies for x in [self.with_taxonomy(tax)] if x.exists())
    def history(self):
        return self.history_of(self.taxonomy_version_map.taxonomy_history())
    def lookup_data(self):
        return self.taxonomy.concept_types.get(self.item_id)
    def exists(self):
        return self.item_id in self.taxonomy.concept_types
    def with_id(self, i):
        return self.with_taxonomy_and_id(self.taxonomy, i)
    def with_taxonomy(self, taxonomy):
        return self.with_taxonomy_and_id(taxonomy, self.item_id)
    def with_taxonomy_and_id(self, taxonomy, i):
        return ConceptType(self.taxonomy_version_map, taxonomy, i)
    def item_type_name(self):
        return 'ConceptType'
    def label_sv(self):
        return self.data().label_sv
    def label_en(self):
        return self.data().label_en
    def collection_accessor(self):
        return accessors.accessor_concept_types

class Concept(codegen_utils.ConceptBase):
    def __init__(self, taxonomy_version_map, taxonomy, item_id):
        assert(isinstance(item_id, str))
        self.taxonomy_version_map = taxonomy_version_map
        self.taxonomy = taxonomy
        self.item_id = item_id
        self._data = None
    def history_of(self, taxonomies):
        return (x for tax in taxonomies for x in [self.with_taxonomy(tax)] if x.exists())
    def history(self):
        return self.history_of(self.taxonomy_version_map.taxonomy_history())
    def lookup_data(self):
        return self.taxonomy.concepts.get(self.item_id)
    def exists(self):
        return self.item_id in self.taxonomy.concepts
    def with_id(self, i):
        return self.with_taxonomy_and_id(self.taxonomy, i)
    def with_taxonomy(self, taxonomy):
        return self.with_taxonomy_and_id(taxonomy, self.item_id)
    def with_taxonomy_and_id(self, taxonomy, i):
        return Concept(self.taxonomy_version_map, taxonomy, i)
    def item_type_name(self):
        return 'Concept'
    def hidden_labels(self):
        return self.data().hidden_labels
    def definition(self):
        return self.data().definition
    def preferred_label(self):
        return self.data().preferred_label
    def ssyk_code_2012(self):
        return self.data().ssyk_code_2012
    def eures_nace_code_2007(self):
        return self.data().eures_nace_code_2007
    def type(self):
        return self.data().type
    def related(self):
        return self.with_ids(self.data().related)
    def short_description(self):
        return self.data().short_description
    def possible_combinations(self):
        return self.with_ids(self.data().possible_combinations)
    def esco_uri(self):
        return self.data().esco_uri
    def broad_match(self):
        return self.with_ids(self.data().broad_match)
    def narrower(self):
        return self.with_ids(self.data().narrower)
    def broader(self):
        return self.with_ids(self.data().broader)
    def narrow_match(self):
        return self.with_ids(self.data().narrow_match)
    def eures_code_2014(self):
        return self.data().eures_code_2014
    def exact_match(self):
        return self.with_ids(self.data().exact_match)
    def replaces(self):
        return self.with_ids(self.data().replaces)
    def uri(self):
        return self.data().uri
    def alternative_labels(self):
        return self.data().alternative_labels
    def unlikely_combinations(self):
        return self.with_ids(self.data().unlikely_combinations)
    def substituted_by(self):
        return self.with_ids(self.data().substituted_by)
    def close_match(self):
        return self.with_ids(self.data().close_match)
    def substitutes(self):
        return self.with_ids(self.data().substitutes)
    def collection_accessor(self):
        return accessors.accessor_concepts