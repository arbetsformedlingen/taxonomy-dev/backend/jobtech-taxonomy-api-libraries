import jobtech_taxonomy_library_python.codegen_utils as cgu
import unittest

class CatAccessor:
    def set(self, target, x):
        target.cat = x

    def get(self, source):
        return source.cat

catacc = CatAccessor()

class Target:
    def __init__(self, init):
        self.cat = init
    
class TestCodegenUtils(unittest.TestCase):
    def test_set_unset(self):
        x = Target(None)
        
        a = cgu.SetAccumulator(119, catacc)
        a2 = cgu.UnsetAccumulator(120, catacc)
        
        self.assertEqual(119, a("August"))
        self.assertEqual(None, x.cat)
        a.apply(x)
        self.assertEqual("August", x.cat)

        self.assertEqual(120, a2(3, 4, 5))
        a2.apply(x)
        self.assertEqual(None, x.cat)
        
    def test_add_remove(self):
        x = Target(set())
        
        a = cgu.AddAccumulator(119, catacc)
        a2 = cgu.RemoveAccumulator(120, catacc)
        
        self.assertEqual(119, a("August"))
        self.assertEqual(set(), x.cat)
        a.apply(x)
        self.assertEqual({"August"}, x.cat)

        self.assertEqual(120, a2("August"))
        a2.apply(x)
        self.assertEqual(set(), x.cat)
        
    def test_keep_first(self):
        coll = [1, 1, 3, 5, 2, 4, 8, 3, 119]
        def even(x):
            if x % 2 == 0:
                return str(x)
        self.assertEqual("2", cgu.keep_first(even, coll))
        self.assertEqual("8", cgu.keep_last(even, coll))
        
