from jobtech_taxonomy_library_python.generated.defs import taxonomy_version_map
from pathlib import Path
import json

def load_json(filename):
    with open(filename, "r") as f:
        return json.load(f)

def silent_check(condition, description):
    if not(condition):
        raise RuntimeError(f"FAILED: {description}")
    
def check(condition, description):
    print(description)
    silent_check(condition, description)

def is_non_empty(x):
    if isinstance(x, str):
        return True
    return 0 < len(x)

def normalize_field(field):
    if isinstance(field, str):
        return field
    elif field == None:
        return field
    dst = set()
    for x in field:
        if isinstance(x, str):
            dst.add(x)
        else:
            dst.add(x["id"])
    return dst

def compare_maps(ref_items, loaded):
    check(len(ref_items) == len(loaded), "Check item counts")
    for c in ref_items:
        cid = c["id"]
        concept_data = loaded[cid].data()
        missing_ks = {k for k in c.keys() if not(k in concept_data) and is_non_empty(c[k])}
        silent_check(len(missing_ks) == 0, f"Missing keys: {missing_ks}")
        common_ks = {k for k in c.keys() if k in concept_data}
        for k in common_ks:
            a = normalize_field(c[k])
            b = normalize_field(c[k])
            silent_check(a == b, f"Different values at concept {cid}, field {k}: {a} != {b}")

def specific_check0():            
    taxl = taxonomy_version_map.latest()
    i = 'NMsH_24t_Bmc'
    occ = taxl.get_concept(i)
    check(occ.preferred_label() == '1:e Fartygsingenjör/1:e Maskinist', f"Check the label of {i}")
    bids = {x.id() for x in occ.broader()}
    check(bids == set(['Rsoy_sS6_6c4', '6y4v_isQ_HBr']), f"Check that the broader concepts of {i} are good.")
    check(occ.type() == "occupation-name", "Check that it is an occupation name")

    occ2 = taxonomy_version_map.latest_concept(i)
    check(occ.id() == occ2.id(), "Compare with the latest")
    print(f"occ.taxonomy: {occ.taxonomy}")
    print(f"occ2.taxonomy: {occ2.taxonomy}")
    check(occ.taxonomy == occ2.taxonomy, "Compare with the latest (taxonomy)")

            
def validate():
    check(not(taxonomy_version_map._edited), "Check that the version map has not been edited since last publish")

    for id_set in [taxonomy_version_map.concept_id_set(), taxonomy_version_map.concept_type_id_set()]:
        check(isinstance(id_set, set), "Check that it is a set")
        check(0 < len(id_set), "Check that it contains elements")
    
    root_path = Path(__file__).parent.parent.parent
    index_file = root_path.joinpath("api-data", "index.json")
    index_data = load_json(index_file)
    version_map = {}
    for file_item in index_data:
        version = file_item["version"]
        if not(version in version_map):
            version_map[version] = []
        version_map[version].append(file_item["dst-file"])
    versions = sorted(set(version_map.keys()))

    check(set(versions) == set(taxonomy_version_map.versions), "Check that the version map is OK!")

    history = list(taxonomy_version_map.taxonomy_history())
    check(len(versions) == len(history), "Check the length of taxonomy history")
    check(versions == [h.version for h in history], "Check that the versions of the history are good")

    # For a single concept
    specific_check0()
    
    concept_version_map = {}
    for version in versions:
        print(f"Validate version {version}")
        reference_taxonomy = {k:v
                              for filename in version_map[version]
                              for (k, v) in load_json(root_path.joinpath(filename))["data"].items()}
        taxonomy_for_version = taxonomy_version_map[version]

        ref_concepts = reference_taxonomy["concepts"]
        ref_concept_types = reference_taxonomy["concept_types"]
        compare_maps(ref_concepts, taxonomy_for_version.concepts)
        compare_maps(ref_concept_types, taxonomy_for_version.concept_types)

        for c in ref_concepts:
            cid = c["id"]
            if not(cid in concept_version_map):
                concept_version_map[cid] = []
            concept_version_map[cid].append(version)
    for cid in concept_version_map:
        vseq = list(concept_version_map[cid])
        cseq = taxonomy_version_map.concept_history(cid)
        chist = [v
                 for c in cseq
                 for v in [c.taxonomy.version]
                 if v != None]
        silent_check(vseq == chist, f"Check concept history for {cid}: {vseq} and {chist}")
        for c in cseq:
            silent_check(c.taxonomy_version_map == taxonomy_version_map, "Check that the root taxonomy is correct")
        
        

    print("SUCCESSFULLY VALIDATED")

validate()
