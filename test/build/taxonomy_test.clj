(ns build.taxonomy-test
  (:require [clojure.test :refer :all]
            [build.taxonomy :refer :all]
            [clojure.data.json :as json]
            [clojure.java.io :as io])
  (:import [java.nio.file Files]
           [java.nio.file.attribute FileAttribute]))

(deftest taxonomy-test
  (let [tmpdir (-> "tax"
                   (Files/createTempDirectory (make-array FileAttribute 0))
                   .toFile
                   .getAbsolutePath)
        settings (assoc default-settings :data-root tmpdir)
        jobs (list-download-jobs settings)
        jobs10 (take 10 jobs)
        empty-file-settings (assoc settings :files-to-download [])]
    (is (sequential? jobs))
    (is (= [1 1 2 2 3 3 4 4 5 5]
           (map :version jobs10)))
    (is (= [] (download-batch [])))
    (is (= [] (list-download-jobs empty-file-settings)))
    (download-taxonomy empty-file-settings)
    (is (.exists (download-index-file empty-file-settings)))
    (->> empty-file-settings
         download-index-file
         slurp
         json/read-str
         (= [])
         is)))

(def small-taxonomy {:version 1,
                     :concept-types
                     [{:id "sni-level-3",
                       :label_en "sni-level-3",
                       :label_sv "sni-level-3"}
                      {:id "sni-level-4",
                       :label_en "sni-level-4",
                       :label_sv "sni-level-4"}],
                     :concepts
                     [{:id "1gru_vL5_k4m",
                       :preferred_label
                       "Partihandel med informations- och kommunikationsutrustning",
                       :narrower [{:id "2e6y_2BU_y8p"} {:id "r2jN_76S_LzD"}],
                       :broader [],
                       :type "sni-level-3"}
                      {:id "2e6y_2BU_y8p",
                       :preferred_label
                       "Partihandel med datorer och kringutrustning samt programvara",
                       :narrower [],
                       :broader [{:id "1gru_vL5_k4m"}],
                       :type "sni-level-4"}
                      {:id "r2jN_76S_LzD",
                       :preferred_label
                       "Partihandel med elektronik- och telekommunikationsutrustning",
                       :narrower [],
                       :broader [{:id "1gru_vL5_k4m"}],
                       :type "sni-level-4"}]})

(deftest taxonomy-load-data-test
  (let [tmpdir (-> "tax"
                   (Files/createTempDirectory (make-array FileAttribute 0))
                   .toFile
                   .getAbsolutePath)
        cfile (io/file tmpdir "concepts.json")
        tfile (io/file tmpdir "concept_types.json")
        vdata {:version 1
               :files {:types tfile
                       :concepts cfile}}
        _ (spit cfile (json/write-str {:data {:concepts (:concepts small-taxonomy)}}))
        _ (spit tfile (json/write-str {:data {:concept-types (:concept-types small-taxonomy)}}))
        tax (load-taxonomy-data vdata)]
    (is (= tax small-taxonomy))))

(deftest taxonomy-transduce-concepts-test
  (let [tax (transduce-concepts (remove (comp #{"r2jN_76S_LzD"} :id)) small-taxonomy)
        cmap (into {} (map (juxt :id identity)) (:concepts tax))]
    (-> cmap
        keys
        set
        (= #{"2e6y_2BU_y8p" "1gru_vL5_k4m"})
        is)
    (-> cmap
        (get "1gru_vL5_k4m")
        :narrower
        (= [{:id "2e6y_2BU_y8p"}])
        is)))
