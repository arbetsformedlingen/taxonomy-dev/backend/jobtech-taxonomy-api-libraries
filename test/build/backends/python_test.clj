(ns build.backends.python-test
  (:require [build.backends.python :refer :all]
            [clojure.test :refer :all]
            [build.code-renderer :as cr]
            [clojure.string :as cljstr]
            [build.analyzer :as analyzer]))

(deftest python-test
  (is (= (:rendered-string (cr/render-string (make-import "x" "y")))
         "\nimport jobtech_taxonomy_library_python.x.y as y"))
  (is (= "concept_type" (python-name :concept-type)))
  (is (= "concept_type" (python-name "concept-type")))
  (is (= "\"hej\"" (python-string-lit "hej")))
  (let [field (-> analyzer/known-fields
                  :concept-types
                  first)
        d (codegen-data-for-field field)]
    (is (map? d))
    (is (contains? d :field-name))
    (is (contains? d :ops))
    (is (sequential? (init-acc-expr field))))
  (let [c (produce-edit-classes)
        s (-> c cr/render-string :rendered-string)]
    (is (string? s))
    (is (< 0 (cljstr/index-of s "Accumulator"))))
  (let [events [{:id "employment-duration",
                 :collection-key :concept-types,
                 :type :create,
                 :edits
                 [{:value-type :string,
                   :cardinality :single,
                   :type :set,
                   :value "employment-duration",
                   :field-key :label_sv}
                  {:value-type :string,
                   :cardinality :single,
                   :type :set,
                   :value "employment-duration",
                   :field-key :label_en}]}]
        expr (produce-history-expression events)
        r (-> expr cr/render-string :rendered-string)]
    (is (#{"[t(s0).ls(s0).le(s0)]" "[t(s0).le(s0).ls(s0)]"} r))
    (let [source (render-data-source {:events events})
          code (:code source)]
      (is (= #{:code :char-freqs} (set (keys source))))
      (is (string? code))
      (is (cljstr/includes? code "build_taxonomy"))
      (is (cljstr/includes? code "history"))
      (is (cljstr/includes? code "s0="))
      (is (cljstr/includes? code "(s0)")))
    (let [a (render-accessors)]
      (is (string? a))
      (is (cljstr/includes? a "accessor_replaces = Accessor_replaces()"))
      (is (cljstr/includes? a "class Accessor_replaces:")))
    (let [m (render-models)]
      (is (string? m))
      (is (cljstr/includes? m "class Concept"))
      (is (cljstr/includes? m "def preferred_label")))))
