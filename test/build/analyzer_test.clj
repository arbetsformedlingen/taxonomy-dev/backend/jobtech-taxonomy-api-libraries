(ns build.analyzer-test
  (:require [clojure.test :refer :all]
            [build.analyzer :refer :all]))

(deftest codegen-test
  (is (id-ref? {:id "abc"}))
  (is (not (id-ref? {})))
  (is (not (id-ref? 9)))
  (is (= :string (derive-field-type "asdf")))
  (is (= :unknown (derive-field-type nil)))
  (is (= :unknown (derive-field-type [])))
  (is (= :string-set (derive-field-type #{"sdf"})))
  (is (= :string-set (derive-field-type ["sdf"])))  
  (is (= :id-set (derive-field-type #{{:id "asdf"}})))
  (is (= :id-set (derive-field-type [{:id "asdf"}])))
  (is (= [{:type :set
           :value "katt"
           :value-type :string
           :cardinality :single}]
         (single-value-edit :string nil "katt")))
  (is (= [{:type :unset
           :value-type :string
           :cardinality :single}]
         (single-value-edit :string "katt" nil)))
  (is (= #{{:type :add
            :cardinality :multiple
            :value-type :string
            :value "lokalvårdare"}
           {:type :remove
            :cardinality :multiple
            :value-type :string
            :value "städerska"}}
         (set (compare-sets :string identity ["städerska"] ["lokalvårdare"]))))
  (is (= [{:type :add
           :cardinality :multiple
           :value-type :id
           :value "324_sdf_sdf"}]
         (compare-sets :id :id [] [{:id "324_sdf_sdf"}])))
  (is (= [{:type :add
           :field-key :broader
           :cardinality :multiple
           :value-type :id
           :value "119"}]
         (field-edit-events :broader [] [{:id "119"}])))
  (is (= {:id "xyz"
          :collection-key :concepts
          :type :create
          :edits [{:type :add
                   :field-key :broader
                   :cardinality :multiple
                   :value-type :id
                   :value "119"}]}
         (edit-event :concepts
                     "xyz"
                     nil
                     {:id "xyz"
                      :broader [{:id "119"}]})))
  (is (= [{:id "xyz"
           :collection-key :concepts
           :type :create
           :edits [{:type :add
                    :field-key :broader
                    :cardinality :multiple
                    :value-type :id
                    :value "119"}]}
          {:type :complete-version
           :version 1000}]
         (taxonomy-edit-events
          [{:version 999
            :concepts []
            :types []}
           {:version 1000
            :concepts [{:id "xyz"
                        :broader [{:id "119"}]}]
            :types []}])))
  (is (map? (analyze-edit-events [{:id "xyz"
                                   :collection-key :concepts
                                   :type :create
                                   :edits [{:type :add
                                            :field-key :broader
                                            :cardinality :multiple
                                            :value-type :id
                                            :value "119"}]}
                                  {:type :complete-version
                                   :version 1000}]))))
