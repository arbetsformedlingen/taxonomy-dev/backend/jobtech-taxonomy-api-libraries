(ns build.code-renderer
  "Utilities for rendering source code")

(declare collect-string-literals)

(defn char-range [first-digit last-digit]
  (for [i (range (int first-digit) (inc (int last-digit)))]
    (char i)))

(def varname-digits (into [] cat [(char-range \0 \9)
                                  (char-range \a \z)
                                  (char-range \A \Z)]))

(defn number-in-base [base-digits number]
  {:pre [(number? number)
         (<= 0 number)]}
  (let [base-digits (apply str (vec base-digits))
        n (count base-digits)]
    (if (zero? number)
      (str (first base-digits))
      (loop [number number
             dst ""]
        (if (zero? number)
          dst
          (recur (quot number n)
                 (str (nth base-digits (rem number n)) dst)))))))

(defn vec-expr [key arg-preds rest-count]
  (let [nargs (count arg-preds)]
    (fn [v]
      (when (vector? v)
        (let [[k & r] v]
          (when (and (= k key)
                   (<= nargs (count r))
                   (or (nil? rest-count) (= (count r) (+ rest-count nargs))))
            (let [[args data] (split-at nargs r)]
              (when (every? identity (map (fn [f x] (f x)) arg-preds args))
                [args data]))))))))

(defn append [^StringBuilder builder s]
  (.append builder s)
  builder)

(defmacro rs-handler [expr & body]
  `(fn [~'rec ~'context ~'dst ~expr] ~@body))

(def rs-syntax [;; Strings: append them to the output
                {:match #(when (string? %) %)
                 :colstr (rs-handler e dst)
                 :render (rs-handler e (append dst e))}

                ;; Expressions on the form [:str s]:
                ;; Treat `s` as a string literal and possibly insert the name of a generated constant for that string.
                {:match (vec-expr :str [string?] 0)
                 :colstr (rs-handler [[s] _] (conj dst s))
                 :render (rs-handler [[s] _] (append dst (get-in context [:varname-map s] s)))}

                ;; Expressions on the form [:prefix p & r]:
                ;; Append `p` to the prefix within this context and render `r` with the updated context.
                {:match (vec-expr :prefix [string?] nil)
                 :colstr (rs-handler [_ r] (rec context r))
                 :render (rs-handler [[p] r] (rec (update context :prefix str p) r))}

                ;; The keyword newline: Insert a newline with the accumulated prefices of the context.
                {:match #(when (#{:newline} %) %)
                 :colstr (rs-handler _ dst)
                 :render (rs-handler _ (append dst (:prefix context)))}

                ;; Any sequential value: Render the elements inside.
                {:match #(when (sequential? %) %)
                 :colstr (rs-handler r (rec context r))
                 :render (rs-handler r (rec context r))}])

(defn traverse-rs-expr [handler-key context dst expr]
  (let [rec (fn [c e] (reduce (fn [dst x]
                                (traverse-rs-expr handler-key c dst x))
                              dst e))]
    (loop [syntax rs-syntax]
      (if (empty? syntax)
        (throw (ex-info "Syntax error" {:expr expr}))
        (let [[s & syntax] syntax
              match (:match s)
              handler (handler-key s)]
          (if-let [matched-data (match expr)]
            (handler rec context dst matched-data)
            (recur syntax)))))))

(defn collect-string-literals [expr]
  (traverse-rs-expr :colstr nil [] expr))

(defn render-string-sub [context builder expr]
  (traverse-rs-expr :render context builder expr))

(defn basic-varname-generator [lit-count-pairs]
  (for [[i [literal n]] (map-indexed vector lit-count-pairs)
        :when (< 1 n)]
    [literal (str "s" (number-in-base varname-digits i))]))

(def default-render-string-settings {:varname-generator basic-varname-generator})

(defn render-string
  "Produces a string from an input of nested vectors inspired by hiccup. The syntax is defined by `rs-syntax` above."
  ([expr] (render-string expr default-render-string-settings))
  ([expr {:keys [varname-generator]}]
   (let [literals (collect-string-literals expr)
         literal-count-pairs (->> literals
                                  frequencies
                                  (sort-by (comp - second)))
         varname-map (into {} (varname-generator literal-count-pairs))
         builder (StringBuilder.)
         context {:varname-map varname-map
                  :prefix "\n"}
         s (render-string-sub context builder expr)]
     {:rendered-string (str s)
      :varname-map varname-map
      :all-literals literals})))

(defn join-coll [sep c]
  {:pre [(coll? c)]}
  (rest (for [x c
              y [sep x]]
          y)))

(defn lines [args]
  {:pre [(sequential? args)]}
  (for [arg args
        x [:newline arg]]
    x))

(defn newlines [n]
  (repeat n :newline))
