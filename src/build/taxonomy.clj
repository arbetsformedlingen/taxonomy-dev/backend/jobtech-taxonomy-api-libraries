(ns build.taxonomy
  "This namespace contains functions for downloading the taxonomy in the form of json files and producing an index of those files. It also contains functions for loading the downloaded data."
  (:require [clj-http.client :as client]
            [clojure.java.io :as io]
            [clojure.string :as cljstr]
            [clojure.data.json :as json]))

(def default-settings {:djs-url "https://data.jobtechdev.se"
                       :data-root "api-data"
                       :parallel-download-count 8
                       
                       :files-to-download
                       [{:type :types
                         :src-path "/taxonomy/version/%d/query/concept-types/concept-types.json"}
                        {:type :concepts
                         :src-path "/taxonomy/version/%d/query/concepts-and-common-relations/concepts-and-common-relations.json"}]})

(defn download-index-file
  "Returns the `java.io.File` where the index of downloaded files is stored."
  [settings]
  (io/file (:data-root settings) "index.json"))

(defn list-download-jobs
  "Returns a sequence where each item specifies what should be downloaded and where it should be saved."
  [{:keys [djs-url data-root files-to-download]}]
  (if (empty? files-to-download)
    []
    (for [version (range) :when (pos-int? version)
          {:keys [src-path type]} files-to-download]
      (let [src-path (format src-path version)
            src-file (io/file src-path)
            name (.getName src-file)
            dst-file (io/file data-root "version" (str version) name)]
        {:full-url (str djs-url src-path)
         :name name
         :type type
         :version version
         :dst-file dst-file}))))

(defn perform-download-job
  "Downloads the file specified by `job` (returned from `list-download-jobs`). On success, it returns the job. On failure, it returns nil."
  [{:keys [full-url dst-file] :as job}]
  (try
    ;; How to download a file: https://stackoverflow.com/a/11321748/1008794
    (let [response (client/get full-url {:as :stream})]
      (io/make-parents dst-file)      
      (clojure.java.io/copy (:body response) dst-file)
      
      ;; Todo: Use some logging library here.
      (println "Downloaded" (str dst-file))
      job)
    (catch clojure.lang.ExceptionInfo _info
      nil)))

(defn download-batch
  "Download multiple jobs in parallel. Only return the jobs that were successful."
  [jobs]
  (let [launched-jobs (mapv #(future (perform-download-job %)) jobs)]
    (into []
          (comp (map deref)
                (remove nil?))
          launched-jobs)))

(defn download-taxonomy
  "Download as much as possible of the taxonomy"
  [settings]
  (let [parallel-download-count (:parallel-download-count settings)
        jobs (list-download-jobs settings)
        downloaded-files (into []
                               (comp (partition-all parallel-download-count)
                                     (map download-batch)
                                     (take-while seq)
                                     cat)
                               jobs)
        index-data (for [f downloaded-files]
                     (update f :dst-file str))]
    (with-open [file (io/writer (download-index-file settings))]
      (json/write index-data file))
    (println "Done downloading the taxonomy.")))


;;;------- Loading the taxonomy -------
(defn make-taxonomy-versions-from-download-index
  "Transform the data into a sequence of maps ordered by versions with the downloaded files of that version."
  [data]
  (let [groups (->> data
                    (group-by :version)
                    (sort-by first))]
    (for [[version group] groups]
      {:version version
       :files (into {} (for [{:keys [type dst-file]} group]
                         [(keyword type) dst-file]))})))

(defn load-taxonomy-versions 
  "Load a sequence of maps ordered by version, with each map holding the filenames to taxonomy data of that version."
  [index-filename]
  (-> index-filename
      slurp
      (json/read-str :key-fn keyword)
      make-taxonomy-versions-from-download-index))

(defn validate-taxonomy-data
  [{:keys [version concepts concept-types] :as taxonomy-data}]
  (assert (sequential? concepts))
  (assert (sequential? concept-types))
  (assert (integer? version))
  taxonomy-data)

(defn deuglify-keyword [k]
  {:pre [(keyword? k)]}
  (-> k name (cljstr/replace "_" "-") keyword))

(defn load-taxonomy-data
  "Load the data of the taxonomy for a specific version, given an item returned by `load-taxonomy-versions`."
  [{:keys [version files]}]
  (validate-taxonomy-data
   (into {:version version}
         (for [[_ filename] files
               [k v] (-> filename
                         slurp
                         (json/read-str :key-fn keyword)
                         :data)]
           [(deuglify-keyword k) v]))))

(defn empty-taxonomy-data
  "Create empty taxonomy data with specificed `version`."
  [version]
  (validate-taxonomy-data {:version version
                           :concepts []
                           :concept-types []}))

(defn- remove-broken-concept-relations [taxonomy-data]
  (update
   taxonomy-data
   :concepts
   (fn [concepts]
     (let [id-set (into #{} (map :id) concepts)]
       (into []
             (map (fn [concept]
                    (into {}
                          (map (fn [[k field]]
                                 [k (if (and (coll? field) (every? map? field))
                                      (into [] (filter (comp id-set :id)) field)
                                      field)]))
                          concept)))
             concepts)))))

(defn transduce-concepts
  "Apply a transducer to the set of concepts of taxonomy-data and then remove broken concept relations"
  [xform taxonomy-data]
  (remove-broken-concept-relations
   (update taxonomy-data :concepts (partial into [] xform))))



(comment ;; Job definition
  (do
    (def jobs (take 100 (list-download-jobs default-settings)))
    (def job (first jobs))

    ))

(comment
  (do

    (def v (load-taxonomy-versions "api-data/index.json"))
    (def d (load-taxonomy-data (first v)))

    (def test-c (first (filter (comp #{2} count :narrower) (:concepts d))))

    (def ids #{"1gru_vL5_k4m" "2e6y_2BU_y8p" "r2jN_76S_LzD"})


    (def mini-tax (transduce-concepts (comp (filter (comp ids :id))
                                            (map #(select-keys % [:id :preferred_label :narrower :broader :type])))
                                      d))

    (def types (into #{} (map :type) (:concepts mini-tax)))

    (def mini-tax2 (update mini-tax :concept-types (partial filter (comp types :id))))

    
    ))


