(ns build.backends.python
  (:require [build.analyzer :as analyzer]
            [build.taxonomy :as taxonomy]
            [clojure.string :as cljstr]
            [build.code-renderer :as cr]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [clojure.tools.logging :as log]))

(def project-name "jobtech_taxonomy_library_python")
(def codegen-subdir "generated")

(def vspace (cr/newlines 3))

(defn make-import [& module-path]
  [:newline "import " project-name (vec (for [p module-path] ["." p])) " as " (last module-path)])

(defn make-class-data [base-name item-name abbrev]
  (let [class-name-and-base (fn [suffix docstring]
                              {:name (str base-name suffix)
                               :docstring docstring
                               :base (str base-name suffix "Base")})]
    {:accumulator (class-name-and-base
                   "Accumulator"
                   "This class is used by the dsl that expresses the data of the Taxonomy.")
     :data (class-name-and-base
            "Data"
            "This class holds the raw data for an item in the Taxonomy.") 
     :wrapper (class-name-and-base
               ""
               "This class is used to access an item in the Taxonomy.")
     :item-name item-name
     :abbrev abbrev}))

(def model-data {:concept-types (make-class-data "ConceptType" "concept_type" "t")
                 :concepts (make-class-data "Concept" "concept" "c")})




;; Todo: Once we start producing code for other languages
;; the specific syntax of each language can be factored out
;; into some polymorphic object. That way, we can reuse most
;; of the code rendering for OOP languages.
(defn python-name [k]
  (-> k
      name
      (cljstr/replace "-" "_")))

(defn accessor-name [k]
  (str "Accessor_" (python-name k)))

(defn accessor-instance-name [k]
  (str "accessor_" (python-name k)))

(defn indent [& args]
  (into [:prefix "    "] args))

(defn unicode-escape [i]
  [(char i) (format "\\u%04X" i)])

(defn python-string-lit [s]
  (str "\""
       (cljstr/escape s (into {\" "\\\""
                               \\ "\\\\"
                               \newline "\\n"}
                              (map unicode-escape)
                              [13]))
       "\""))

(defn python-block [header body]
  [:newline header ":" (indent body)])

(defn python-method [header body]
  (python-block ["def " header] body))

(defn python-small-method [header body]
  (python-method header [:newline body]))

(def python-function python-method)
(def python-small-function python-small-method)

(defn python-class [header body]
  (python-block ["class " header] body))

(defn python-list [items]
  ["[" (cr/join-coll "," items) "]"])

(defn python-call [name args]
  [name "(" (cr/join-coll "," args) ")"])

(defn python-method-call [name args]
  (python-call ["." name] args))

(defn codegen-data-for-field [[field-key field-data]]
  (let [{:keys [cardinality abbrev]} field-data
        {:keys [init-expr ops]} (case cardinality
                                  :single {:init-expr "None"
                                           :ops [[:add "" "Set"]
                                                 [:remove "_" "Unset"]]}
                                  :multiple {:init-expr "set()"
                                             :ops [[:add "" "Add"]
                                                   [:remove "_" "Remove"]]})]
    {:field-accessor-name (str "accessors." (accessor-instance-name field-key))
     :field-name (python-name field-key)
     :init-expr init-expr
     :ops (into {} (for [[mode prefix op-name] ops]
                     [mode {:accumulator-class-name (str "codegen_utils." op-name "Accumulator")
                            :accumulator-field-name (str prefix abbrev)}]))}))

(defn init-acc-expr [field]
  (let [{:keys [ops field-accessor-name]} (codegen-data-for-field field)]
    (cr/lines
     (for [[op {:keys [accumulator-class-name accumulator-field-name]}] ops]
       (str "self." accumulator-field-name "=" accumulator-class-name "(self, " field-accessor-name ")")))))

(defn collection-accessor-method [coll-key]
  (python-small-method
   "collection_accessor(self)"
   ["return accessors." (accessor-instance-name coll-key)]))

(defn produce-edit-class [[coll-key fields]]
  (let [class-data (get model-data coll-key)
        {:keys [accumulator data]} class-data
        accumulator-name (:name accumulator)
        data-name (:name data)
        abbrev (:abbrev class-data)]
    (assert class-data)
    
    [(python-class
      [accumulator-name "(codegen_utils.ItemAccumulator)"]
      [(python-small-method
        "__init__(self, item_id)"
        (cr/lines
         (into ["self.item_id=item_id"
                "self.deleted=False"]
               (map init-acc-expr)
               fields)))
       (collection-accessor-method coll-key)
       (python-small-method
        "new_data(self)"
        ["return models." data-name "(self.item_id)"])
       (python-small-method
        "accumulators(self)"
        ["return "
         (python-list
          (for [field fields
                [_ {:keys [accumulator-field-name]}] (:ops (codegen-data-for-field field))]
            (str "self." accumulator-field-name)))])
       (python-small-method "delete(self)" "self.deleted=True; return self;")])
     (python-small-function
      [abbrev "(item_id)"]
      ["return " accumulator-name "(item_id)"])
     vspace]))

(defn produce-edit-classes []
  (cr/lines (map produce-edit-class analyzer/known-fields)))

(defn item-event-expr [{:keys [type id collection-key edits]}]
  (let [constructor (get-in model-data [collection-key :abbrev])
        start-expr [constructor "(" [:str (python-string-lit id)] ")"]
        field-data (collection-key analyzer/known-fields)]
    (if (= :delete type)
      [start-expr (python-method-call "delete" [])]
      [start-expr (for [;{:keys [value-type cardinality type new-value field-key]}
                        [field-key field-group] (group-by :field-key edits)
                        [op-type op-group] (group-by :type field-group)]
                    (let [prefix (case op-type
                                   :set ""
                                   :add ""
                                   :remove "_"
                                   :unset "_")
                          abbrev (:abbrev (field-key field-data))]
                      (when (not abbrev)
                        (throw (ex-info "No abbrev"
                                        {:field-key field-key
                                         :field-data field-data
                                         :collection-key collection-key})))
                      (python-method-call
                       [prefix abbrev]
                       (for [edit op-group]
                         (let [v (:value edit)]
                           (assert (string? v))
                           [:str (python-string-lit v)])))))])))


(defn produce-history-expression [events]
  (python-list
   (for [event events]
     (case (:type event)
       :complete-version (format "codegen_utils.CompleteVersion(%d)" (:version event))
       (item-event-expr event)))))

(defn render-item-accumulators [_cg-data]
  (-> [(make-import "codegen_utils")
       (make-import codegen-subdir "models")
       (make-import codegen-subdir "accessors")
       
       vspace
       (-> (produce-edit-classes)
           cr/render-string
           :rendered-string)]
      cr/render-string
      :rendered-string))

(defn render-data-source [cg-data]
  (let [{:keys [events]} cg-data
        {:keys [rendered-string varname-map all-literals]}
        (cr/render-string
         (produce-history-expression events))
        string-defs (apply str (for [[expr varname] varname-map]
                                 (str varname "=" expr ";")))]
    ;; Code
    {:code (:rendered-string
            (cr/render-string
             [(make-import "codegen_utils")
              (make-import codegen-subdir "models")
              [:newline "from " project-name "." codegen-subdir ".item_accumulators import "
               (cr/join-coll ", " (for [[_k v] model-data] (:abbrev v)))]
              vspace
              (python-function
               "build_taxonomy_version_map()"
               [:newline "\"\"\"Construct a taxonomy\"\"\""
                :newline string-defs
                :newline (str "history=" rendered-string)
                :newline "return models.TaxonomyVersionMap().apply_accumulators(history)"])]))
     :char-freqs (frequencies (into [] cat all-literals))}))

(defn render-debug-char-freqs [freqs]
  (cljstr/join
   "\n"
   (into ["# This file is for debugging."
          "# Run it with Python to discover which characters are not properly escaped."]
         (for [[c _n] (sort-by second freqs)]
           (str "char_" (int c) "=" (python-string-lit (str c)))))))

(defn produce-data-source [root-path cg-data]
  (let [data-filename (io/file root-path project-name codegen-subdir "data.py")
        freq-filename (io/file root-path project-name codegen-subdir "chars.py")
        {:keys [code char-freqs]} (render-data-source cg-data)]
    (io/make-parents data-filename)
    (spit freq-filename (render-debug-char-freqs char-freqs))
    (spit data-filename code)))

(defn render-accessors []
  (let [field-set (into #{}
                        cat
                        [(keys model-data)
                         (for [[_coll-key fields] analyzer/known-fields
                               [field-key _field-data] fields]
                           field-key)])]
    (:rendered-string
     (cr/render-string
      (for [field-key field-set]
        [(python-class
          (accessor-name field-key)
          [(python-small-method "name(self)"
                         ["return " (python-string-lit (python-name field-key))])
           (python-small-method "get(self, src)"
                         ["return src." (python-name field-key)])
           (python-small-method "set(self, dst, x)"
                         [(str "dst." (python-name field-key) "=x")])])
         :newline
         (accessor-instance-name field-key) " = " (accessor-name field-key) "()"
         vspace])))))


(defn render-models []
  (let [taxonomy-version-map-model (python-class
                                    "TaxonomyVersionMap(codegen_utils.TaxonomyVersionMapBase)"
                                    [(python-small-method
                                      "__init__(self)"
                                      ["self._initialize(Taxonomy())"])
                                     (for [[k {:keys [item-name wrapper]}] model-data]
                                       (let [wname (:name wrapper)]
                                         [(python-small-method
                                           ["latest_" item-name "(self, i)"]
                                           ["return codegen_utils.keep_last(lambda tax: "
                                            wname "(self, tax, i).exists_or_none()"
                                            ", self.taxonomy_history())"])
                                          (python-small-method
                                           [item-name "_history(self, i)"]
                                           ["return " wname "(self, self.latest, i).history()"])
                                          (python-small-method
                                           [item-name "_id_set(self)"]
                                           ["return {i for tax in self.taxonomy_history() "
                                            "for i in tax." (python-name k) ".keys()}"])]))])
        taxonomy-model [(python-class
                         "Taxonomy(codegen_utils.TaxonomyBase)"
                         [(python-method
                           "__init__(self)"
                           [:newline "self.version = None"
                            (for [[k v] model-data]
                              [:newline "self." (python-name k) "={}"])])

                          (python-method
                           "deep_copy(self)"
                           [:newline "dst = Taxonomy()"
                            :newline "dst.version = self.version"
                            (for [[k v] model-data]
                              (let [f (python-name k)]
                                [:newline "dst." f "= codegen_utils.deep_copy_map(self." f ")"]))
                            :newline "return dst"])
                          
                          (python-small-method
                           "accessors(self)"
                           ["return " (python-list
                                       (for [[k v] model-data]
                                         ["accessors." (accessor-instance-name k)]))])
                          (for [[coll-name {:keys [item-name wrapper]}] model-data]
                            (let [ ;;coll-acc (accessor-instance-name coll-name)
                                  cname (python-name coll-name)
                                  wname (:name wrapper)]
                              [(python-small-method
                                ["get_" item-name "(self, i)"]
                                ["return " wname "(self, self, i)"])
                               (python-small-method
                                [item-name "_keys(self)"]
                                ["return self." cname ".keys()"])
                               (python-small-method
                                [item-name "_list(self)"]
                                ["return [self.get_" item-name "(x) for x in self." cname ".keys()]"])]))])]
        data-models (cr/lines
                     (for [[collection-key fields] analyzer/known-fields]
                       (let [{:keys [data]} (collection-key model-data)]
                         (python-class
                          [(:name data) "(codegen_utils.ItemData)"]
                          [(python-method
                            "__init__(self, item_id)"
                            [:newline "self.item_id=item_id"
                             (for [field fields]
                               (let [{:keys [field-name init-expr]} (codegen-data-for-field field)]
                                 [:newline "self." field-name "=" init-expr]))])
                           (python-small-method
                            "accessors(self)"
                            ["return "
                             (python-list
                              (into [["codegen_utils.ConstantAccessor('id', self.item_id)"]]
                                    (map (comp :field-accessor-name codegen-data-for-field))
                                    fields))])
                           (python-method
                            "deep_copy(self)"
                            [:newline "dst = " (:name data) "(self.item_id)"
                             (for [field fields]
                               (let [{:keys [field-name]} (codegen-data-for-field field)]
                                 [:newline "dst." field-name "=copy(self." field-name ")"]))
                             :newline "return dst"])])
                         )))
        wrapper-models (cr/lines
                        (for [[collection-key fields] analyzer/known-fields]
                          (let [{:keys [wrapper item-name]} (collection-key model-data)]
                            (python-class
                             [(:name wrapper) "(codegen_utils." (:base wrapper) ")"]
                             [(python-method
                               "__init__(self, taxonomy_version_map, taxonomy, item_id)"
                               [:newline "assert(isinstance(item_id, str))"
                                :newline "self.taxonomy_version_map = taxonomy_version_map"
                                :newline "self.taxonomy = taxonomy"
                                :newline "self.item_id = item_id"
                                :newline "self._data = None"])
                              (python-small-method
                               "history_of(self, taxonomies)"
                               ["return (x for tax in taxonomies for x in [self.with_taxonomy(tax)] if x.exists())"])
                              (python-small-method
                               "history(self)"
                               ["return self.history_of(self.taxonomy_version_map.taxonomy_history())"])
                              (python-small-method
                               "lookup_data(self)"
                               ["return self.taxonomy." (python-name collection-key) ".get(self.item_id)"])
                              (python-small-method
                               "exists(self)"
                               ["return self.item_id in self.taxonomy." (python-name collection-key)])
                              (python-small-method
                               "with_id(self, i)"
                               ["return self.with_taxonomy_and_id(self.taxonomy, i)"])
                              (python-small-method
                               "with_taxonomy(self, taxonomy)"
                               ["return self.with_taxonomy_and_id(taxonomy, self.item_id)"])
                              (python-small-method
                               "with_taxonomy_and_id(self, taxonomy, i)"
                               ["return " (:name wrapper) "(self.taxonomy_version_map, taxonomy, i)"])
                              (python-small-method
                               "item_type_name(self)"
                               ["return '" (:name wrapper) "'"])
                              (for [field fields]
                                (let [[_field-key {:keys [value-type cardinality]}] field
                                      {:keys [field-name]} (codegen-data-for-field field)
                                      return-value-expr ["return self.data()." field-name]]
                                  (python-small-method
                                   [field-name "(self)"]
                                   (case [value-type cardinality]
                                     [:string :single] return-value-expr
                                     [:string :multiple] return-value-expr
                                     [:id :multiple] ["return self.with_ids(self.data()." field-name ")"]))))
                              (collection-accessor-method collection-key)]))))]
    (:rendered-string
     (cr/render-string
      (cr/lines
       [(make-import "codegen_utils")
        (make-import codegen-subdir "accessors")
        "from copy import copy"
        taxonomy-version-map-model
        taxonomy-model
        data-models
        wrapper-models])))))

(defn produce-models [root-path _]
  (let [models-filename (io/file root-path project-name codegen-subdir "models.py")]
    (io/make-parents models-filename)
    (spit models-filename (render-models))))

(defn produce-accessors [root-path _]
  (let [accessor-filename (io/file root-path project-name codegen-subdir "accessors.py")]
    (io/make-parents accessor-filename)
    (spit accessor-filename (render-accessors))))

(defn produce-item-accumulators [root-path cg-data]
  (let [filename (io/file root-path project-name codegen-subdir "item_accumulators.py")]
    (io/make-parents filename)
    (spit filename (render-item-accumulators cg-data))))

(defn produce-defs [root-path _]
  (let [filename (io/file root-path project-name codegen-subdir "defs.py")]
    (io/make-parents filename)
    (spit filename (->> [(make-import codegen-subdir "data")
                         vspace
                         "taxonomy_version_map = data.build_taxonomy_version_map()"]
                        cr/render-string
                        :rendered-string))))

(defn produce-code [output-project-root cg-data]
  (log/info "Produce Python code")
  (doseq [renderer [produce-models
                    produce-accessors
                    produce-item-accumulators
                    produce-data-source
                    produce-defs]]
    (renderer output-project-root cg-data))
  (log/info "Done producing Python code."))

(defn has-poetry? []
  (-> "poetry"
      shell/sh
      :exit
      zero?))

(defn validate-implementation [output-project-root]
  (if (has-poetry?)
    (let [cmd ["poetry" "run" "python3" "tests/validate_implementation.py" :dir output-project-root]]
      (log/info "Validating Python implementation")
      (let [result (apply shell/sh cmd)]
        (if (zero? (:exit result))
          []
          (do (log/error "Validation of Python implementation failed")
              [(assoc result
                      :message "Validation failed"
                      :command cmd)]))))
    (do (log/warn "Cannot validate because Poetry is not installed")
        [])))

(defn build-and-validate [output-project-root cg-data]
  (produce-code output-project-root cg-data)
  (validate-implementation output-project-root))

(comment

  (defn demo [d]
    (produce-code "taxonomy-api-python" d))
  
  
  )

(comment



  (produce-code "taxonomy-api-python" cg-data)

    )

(comment
  (do


    (def cg-data (-> taxonomy/default-settings
                     taxonomy/download-index-file
                     taxonomy/load-taxonomy-versions
                     analyzer/build-codegen-data))

    (def analysis (:analysis cg-data))
    (def events (:events cg-data))

    (def cg-data0 (analyzer/sparse-sample cg-data 500))

    ))

(comment
  (do

    (defn slice-at [s index]
      (subs s
            (max 0 (- index 100))
            (min (count s) (+ index 100))))

    (def edits (for [event events
                     edit (:edits event)]
                 edit))
    (def result (render-data-source cg-data0))
    (def s (:code result))
    
    (println (subs s 0 1000))
    ))


