(ns build.analyzer
  "This namespace contains functions for analyzing the raw downloaded taxonomy data and transform it into data that can be used for generating code. The top level function for producing data that can be used for code generation is called `build-codegen-data`."
  (:require [build.taxonomy :as taxonomy]))


(defn id-ref? [x]
  (and (map? x) (contains? x :id)))

(defn derive-field-type [x]
  (cond
    (string? x) :string
    (nil? x) :unknown
    (and (coll? x) (empty? x)) :unknown
    (and (coll? x) (every? string? x)) :string-set
    (and (coll? x) (every? id-ref? x)) :id-set
    :else (throw (ex-info "Cannot derive field type" {:x x}))))

(defn compare-sets [value-type unwrapper before after]
  (let [before (into #{} (map unwrapper) before)
        after (into #{} (map unwrapper) after)
        removed (into #{} (remove after) before)
        added (into #{} (remove before) after)]
    (for [[op values] [[:add added]
                       [:remove removed]]
          value (sort values)]
      {:type op
       :cardinality :multiple
       :value-type value-type
       :value value})))

(defn single-value-edit [value-type _ after]
  [(merge {:value-type value-type
           :cardinality :single}
          (if (some? after)
            {:type :set :value after}
            {:type :unset}))])

(defn field-edit-events
  "Compares the field of a concept between two versions."
  [k before after]
  []
  (let [[field-type & r] (into #{}
                               (comp (map derive-field-type)
                                     (remove #{:unknown}))
                               [before after])]
    (if (nil? field-type) ;; This happens for example when `before` is `nil` and `after` is `[]`.
      []
      (do (when (seq? r)
            (throw (ex-info "Cannot derive common field type"
                            {:before before
                             :after after})))
          (map
           #(assoc % :field-key k)
           (case field-type
             :string (single-value-edit :string before after)
             :string-set (compare-sets :string identity before after)
             :id-set (compare-sets :id :id before after)))))))

(defn validate-edit [{:keys [type field-key value-type] :as edit}]
  (assert (keyword? type))
  (assert (keyword? field-key))
  (assert (keyword? value-type))
  edit)


(defn new-event-data "Produce event data for creating a new item in a taxonomy, e.g. a concept or a type."
  [edit-type before after]
  {:pre [(or (map? before) (nil? before))
         (or (map? after) (nil? after))]}
  {:type edit-type
   :edits (vec (let [ks (into #{}
                              (comp (mapcat keys) (remove #{:id}))
                              [before after])]
                 (for [k (sort ks)
                       :let [k-before (get before k)
                             k-after (get after k)]
                       :when (not= k-before k-after)
                       event (field-edit-events k k-before k-after)]
                   event)))})

(defn edit-event
  "Given two corresponding items `before` and from the next version `after`, produce edit data that reflects the modifications from one to the next. The items can be two concepts or two types."
  [collection-key id before after]
  (merge {:id id
          :collection-key collection-key}
         (case [(some? before) (some? after)]
           [true false] {:type :delete}
           [false true] (new-event-data :create before after)
           [true true] (new-event-data :update before after))))

(defn edit-events-for-collections-at-key
  "Produce edit events for a collection of concepts or types between two versions of the taxonomy."
  [k before after]
  (let [before-map (into {} (map (juxt :id identity)) (before k))
        after-map (into {} (map (juxt :id identity)) (after k))
        all-ids (into #{} (mapcat keys) [before-map after-map])]
    (for [id (sort all-ids)
          :let [item-before (before-map id)
                item-after (after-map id)]
          :when (not= item-after item-before)]
      (edit-event k id item-before item-after))))

(defn taxonomy-edit-events
  "Given a pair of taxonomys for different versions, produce 'edit events' to build the `after` taxonomy starting from `before`."
  [[before after]]
  (into []
        cat
        [(edit-events-for-collections-at-key :concept-types before after)
         (edit-events-for-collections-at-key :concepts before after)
         [{:type :complete-version
           :version (:version after)}]]))

(defn edit-events 
  "Given `version-data` previously loaded using `taxonomy/load-taxonomy-version`, produce a sequence of 'edit events' that represents the history of taxonomy from the first version to the last version. This sequence concisely represents the taxonomy and can be used to reconstruct all versions of the taxonomy in the spirit of 'event-sourcing'."
  [version-data xform-load-taxonomy]
  (let [start-version (-> version-data first :version dec)]
    (for [pair (partition 2 1 (cons (taxonomy/empty-taxonomy-data start-version)
                                    (eduction xform-load-taxonomy version-data)))
          event (taxonomy-edit-events pair)]
      event)))

(def known-event-types #{:create :update :delete :complete-version})
(def known-collection-keys #{:concepts :concept-types})

(defn summarize-fields
  "Given a set of events, return a set that summarizes the kind of fields referred to by the events."
  [events]
  (into #{}
        (map (fn [[k v]] (assoc k :count v)))
        (frequencies
         (for [event events
               edit (:edits event)]
           (select-keys edit [:field-key :value-type :cardinality])))))

(def known-fields
  {:concept-types
   {:label_sv {:value-type :string, :cardinality :single :abbrev "ls"},
    :label_en {:value-type :string, :cardinality :single :abbrev "le"}},
   :concepts
   {:hidden_labels {:value-type :string, :cardinality :multiple :abbrev "hl"},
    :definition {:value-type :string, :cardinality :single :abbrev "df"},
    :preferred_label {:value-type :string, :cardinality :single :abbrev "p"},
    :ssyk_code_2012 {:value-type :string, :cardinality :single :abbrev "sc2012"},
    :eures_nace_code_2007 {:value-type :string, :cardinality :single :abbrev "enc2007"},
    :type {:value-type :string, :cardinality :single :abbrev "t"},
    :related {:value-type :id, :cardinality :multiple :abbrev "r"},
    :short_description {:value-type :string, :cardinality :single :abbrev "sd"},
    :possible_combinations {:value-type :id, :cardinality :multiple :abbrev "pc"},
    :esco_uri {:value-type :string, :cardinality :single :abbrev "eu"},
    :broad_match {:value-type :id, :cardinality :multiple :abbrev "bm"},
    :narrower {:value-type :id, :cardinality :multiple :abbrev "n"},
    :broader {:value-type :id, :cardinality :multiple :abbrev "b"},
    :narrow_match {:value-type :id, :cardinality :multiple :abbrev "nm"},
    :eures_code_2014 {:value-type :string, :cardinality :single :abbrev "ec2014"},
    :exact_match {:value-type :id, :cardinality :multiple :abbrev "em"},
    :replaces {:value-type :id, :cardinality :multiple :abbrev "rp"},
    :uri {:value-type :string, :cardinality :single :abbrev "u"},
    :alternative_labels {:value-type :string, :cardinality :multiple :abbrev "a"},
    :unlikely_combinations {:value-type :id, :cardinality :multiple :abbrev "uc"},
    :substituted_by {:value-type :id, :cardinality :multiple :abbrev "sy"},
    :close_match {:value-type :id, :cardinality :multiple :abbrev "cm"},
    :substitutes {:value-type :id, :cardinality :multiple :abbrev "s"}}})

(defn validate-known-fields [known-fields]
  ;; First check that the known-fields are OK
  (doseq [[_coll-key fields] known-fields]

    ;; Check that two fields don't have the same abbreviation.
    (doseq [[k fields] (group-by (comp :abbrev second) fields)]
      (assert (string? k))
      (assert (seq k))
      (when (not= 1 (count fields))
        (throw (ex-info "Same abbreviation" {:abbrev k}))))))

(defn field-type-data [field]
  (select-keys field [:value-type :cardinality]))

(defn analyze-edit-events
  "Go through all edit events to derive invariants and find inconsistencies."
  [events]
  (validate-known-fields known-fields)
  (let [event-types (into #{} (map :type) events)
        _ (assert (every? known-event-types event-types))
        item-events (into [] (filter (comp #{:create :update :delete} :type)) events)
        collection-keys (into #{} (map :collection-key) item-events)
        _ (assert (every? known-collection-keys collection-keys))

        ;; First analyze all the item events to see what fields there are.
        fields-per-collection (into {} (for [[coll-key events] (group-by :collection-key item-events)]
                                         [coll-key (into {}
                                                         (map (fn [[field-key field-data]]
                                                                (when (not= 1 (count field-data))
                                                                  (throw (ex-info "Ambiguous field"
                                                                                  {:collection-key coll-key
                                                                                   :fields field-data})))
                                                                (let [field (first field-data)]
                                                                  [field-key (dissoc field :field-key)])))
                                                         (group-by :field-key (summarize-fields events)))]))]

    ;; Check that the fields correspond to the known fields
    (doseq [[coll-key fields] fields-per-collection
            [field-key field-data] fields]
      (let [known-field (-> known-fields coll-key field-key)]
        (when (not= (field-type-data field-data)
                    (field-type-data known-field))
          (throw (ex-info "Unexpected field data"
                          {:collection-key coll-key
                           :field-key field-key
                           :data field-data
                           :expected known-field})))))

    {:event-types event-types
     :fields-per-collection fields-per-collection}))

(defn codegen-data? [x]
  (and (map? x)
       (contains? x :version-data)
       (contains? x :events)
       (contains? x :analysis)))

(def default-settings {:xform-load-taxonomy (map taxonomy/load-taxonomy-data)})

(defn build-codegen-data
  "Precompute all data needed for code generation in various languages."
  ([version-data] (build-codegen-data version-data default-settings))
  ([version-data {:keys [xform-load-taxonomy]}]
   {:post [(codegen-data? %)]}
   (let [events (edit-events version-data xform-load-taxonomy)
         analysis (analyze-edit-events events)]
     {:version-data version-data
      :events events
      :analysis analysis})))

(defn sparse-sample
  "Use this function to make a new data structure with sampled events"
  [codegen-data n]  
  (update codegen-data :events #(take-nth n %)))


(comment


  (def cg-data (-> taxonomy/default-settings
                   taxonomy/download-index-file
                   taxonomy/load-taxonomy-versions
                   build-codegen-data))

  )

(comment ;; Loading the data
  (do

    (def tax (-> taxonomy/default-settings
                 taxonomy/download-index-file
                 taxonomy/load-taxonomy-versions))
    (def v1 (first tax))
    (def d (taxonomy/load-taxonomy-data v1))
    (def e (vec (edit-events tax (:xform-load-taxonomy default-settings))))

    (def ec (first (filter (comp #{:create} :edit-type) (reverse e))))
    
    ))

