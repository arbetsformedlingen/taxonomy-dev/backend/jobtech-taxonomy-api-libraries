﻿namespace JobtechTaxonomy

open FSharp.Data

module Metadata =
  let Version = 1
  let Name = "JobtechDev Taxonomy^TM"

module Taxonomy =
  [<Literal>]
  let sampleUrl = "./data.json" 
  type Taxonomy = JsonProvider<sampleUrl>
  let data = Taxonomy.GetSample()
  