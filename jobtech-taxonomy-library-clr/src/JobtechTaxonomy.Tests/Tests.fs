module Tests

open Xunit
open Swensen.Unquote
open Hedgehog

open JobtechTaxonomy

[<Fact>]
let ``My test`` () =
    test <@ Metadata.Version = 1 @>

[<Fact>]
let ``My otter test`` () =
    test <@ Metadata.Name = "JobtechDev Taxonomy^TM" @>

[<Fact>]
let ``My taxonomy test`` () =
    test <@ Taxonomy.sampleUrl = "./data.json" @>

[<Fact>]
let ``My taxonomy test`` () =
    test <@ Taxonomy.data = 1 @>

